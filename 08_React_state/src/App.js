import './App.css';
import {useState} from "react";

const App = () => {
  /*
  * https://zh-hans.reactjs.org/docs/hooks-reference.html#usestate
  * 上面是 useState 的官方API地址
  * */
  const [counter, setCounter] = useState(100);
  const handlerAdd = () => {
    setCounter(counter + 1);
  }
  const handlerSubtract = () => {
    setCounter(prev => prev - 1);
  }
  return <div className={'app'}>
    <h1>{counter}</h1>
    <button onClick={handlerSubtract} >-</button>
    <button onClick={handlerAdd}>+</button>
  </div>
};

export default App;