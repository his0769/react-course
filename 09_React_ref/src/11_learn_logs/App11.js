import Logs from "./Components/Logs/Logs";
import './App11.css';
import LogsForm from "./Components/LogsForm/LogsForm";
const App11 = () => {
  const saveLogHandler = (payload) => {
    console.log("App.js -> ", payload);
  };
  return <div className={'app'}>
    <LogsForm onSaveLog={saveLogHandler}></LogsForm>
    <Logs></Logs>
  </div>;
};

export default App11;
