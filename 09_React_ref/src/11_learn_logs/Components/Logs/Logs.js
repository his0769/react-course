
import LogItem from "./LogItem/LogItem";
import './Logs.css';
import Card from "../UI/Card/Card";

/* 日志容器 */
const Logs = () => {
  return <Card className='logs'>
    <LogItem></LogItem>
    <LogItem></LogItem>
    <LogItem></LogItem>
  </Card>;
};

export default Logs;
