import React from 'react';
import MyDate from "./MyDate/MyDate";
import './LogItem.css';
import Card from "../../UI/Card/Card";
/*
  rsc 可以快捷的生成模板
*/
const LogItem = () => {
  return (
    <Card className='item'>
      <MyDate></MyDate>
      {/*日志内容容器*/}
      <div className='content'>
        <h2 className='desc'>学习React</h2>
        <div className="time">40分钟</div>
      </div>
    </Card>
  );
};

export default LogItem;
