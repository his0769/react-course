import React from 'react';
import './Card.css';

const Card = (props) => {
  // console.log(props.children)
  /**
   * className={`card ${props.className}`} : 将设置在组件 Card 上的样式拷贝到当前组件中
   */
  return (
    <div className={`card ${props.className}`}>
      {props.children}
    </div>
  );
};

export default Card;