import React, {useRef} from 'react';
import Card from "../UI/Card/Card";
import './LogsForm.css';

const LogsForm = (props) => {
  const btnClickHandler = (event) => {
    event.preventDefault();
    props.onSaveLog({desc: 'hello', time: 20,});
  }
  const descRef = useRef();
  // const descChangeHandler = () => {
  //   console.log(descRef.current.value);
  // };
  const descChangeHandler = (event) => {
    console.log(event);
  };

  return (
    <Card className={'logs-form'}>
      <form>
        <div className={'form-item'}>
          <label htmlFor="date">日期</label>
          <input id={'date'} type="date"/>
        </div>
        <div className={'form-item'}>
          <label htmlFor="desc">内容</label>
          {/*<input ref={descRef} onChange={descChangeHandler} id={'desc'} type="text"/>*/}
          <input ref={descRef} onChange={descChangeHandler} id={'desc'} type="text"/>
        </div>
        <div className={'form-item'}>
          <label htmlFor="time">时长</label>
          <input id={'time'} type="number"/>
        </div>
        <div className={'form-btn'}>
          <button onClick={btnClickHandler}>添加</button>
        </div>
      </form>
    </Card>
  );
};

export default LogsForm;