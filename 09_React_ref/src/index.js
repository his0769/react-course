import ReactDOM from 'react-dom/client';
import App12 from "./12_context/App12";
// import App09d01 from "./09_Ref/d01/App09d01";
// import App10d01 from "./10_class_component/d01/App10d01";
// import App11 from './11_learn_logs/App11';
const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<App12/>);
