import React from 'react';
import TextContext from "../store/TextContext";
import A from "./A";
import A2 from "./A2";

const TestWrapper = () => {
  return (
    <TextContext.Provider value={{name: '猪八戒', age: 28}}>
      <A></A>
      <A2></A2>
    </TextContext.Provider>
  );
};

export default TestWrapper;