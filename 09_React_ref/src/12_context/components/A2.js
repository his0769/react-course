import React, {useContext} from 'react';
import TextContext from '../store/TextContext';

/**
 * 第二种使用方式; 使用钩子函数
 * useContext
 *
 * @returns {JSX.Element}
 * @constructor
 */
const A2 = () => {
  const ctx = useContext(TextContext);
  return (
    <div>
      {`${ctx.name} -- ${ctx.age}`}
    </div>
  );
};

export default A2;