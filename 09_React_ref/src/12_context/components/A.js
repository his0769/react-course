import React from 'react';
import TextContext from '../store/TextContext';

/**
 * 第一种使用方式
 * <TextContext.Consumer> 需要一个回调函数
 *
 * @returns {JSX.Element}
 * @constructor
 */
const A = () => {
  return (
    <TextContext.Consumer>
      {(ctx)=>{
        return <div>
          {`${ctx.name} -- ${ctx.age}`}
        </div>
      }}
    </TextContext.Consumer>
  );
};

export default A;