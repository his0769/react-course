import React from 'react';
import A from "./components/A";
import A2 from "./components/A2";
import TestWrapper from "./components/TestWrapper";

const App12 = () => {
  return (
    <div>
      hello
      <A></A>
      <A2></A2>
      <TestWrapper></TestWrapper>
    </div>
  );
};

export default App12;