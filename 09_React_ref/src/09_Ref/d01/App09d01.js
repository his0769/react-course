import './App09d01.css';
import {useRef, useState} from "react";

let temp;
const App09d01 = () => {
  /*
  * ① React中的钩子函数只能用于函数组件或自定义钩子函数中；不能用于React类继承中
  * ② 钩子函数只能直接在函数组件中调用
  *
  * useRef : https://zh-hans.reactjs.org/docs/hooks-reference.html#useref
  *
  * */
  const h1Ref = useRef();
  // const h1Ref = {current: undefined};
  console.log(temp == h1Ref);
  temp = h1Ref;
  const clickHandler = () => {
    const header = document.getElementById('header');
    console.log(h1Ref.current);
    // alert(h1Ref.current === header)
  }
  const [counter, setCounter] = useState(10);
  const counterAddHandler = () => {
    setCounter(prev => prev + 1);
  };
  return <div className={'app'}>
    <h1 id={'header'} ref={h1Ref}>hello{counter}</h1>
    <button onClick={clickHandler}>1</button>
    <button onClick={counterAddHandler}>2</button>
  </div>
};

export default App09d01;