import React, {Component} from 'react';

class User extends Component {

  state = {
    counter: 1
  };

  // 只能用箭头函数，否则拿不到`this`
  clickHandler = () => {
    // console.log(this.divRef);
    // this.setState({counter: this.state.counter + 1});
    this.setState(prevState => {
      return {
        counter: prevState.counter + 1
      };
    })
  };

  divRef = React.createRef();
  /**
   * 类组件的 props 可以直接通过 this.props 访问
   *
   * @returns {JSX.Element}
   */
  render() {
    return (
      <div ref={this.divRef}>
        <h1>{this.state.counter}</h1>
        <ul>
          <li>姓名：{this.props.name}</li>
          <li>年龄：{this.props.age}</li>
          <li>性别：{this.props.gender}</li>
        </ul>
        <button onClick={this.clickHandler}>click</button>
      </div>
    );
  }
}

export default User;