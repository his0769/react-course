
import './App10d01.css';
import React from "react";
import User from './components/User/User';

/**
 * WebStorm 快捷键
 * rsc -> 函数组件
 * rsi -> 函数组件 带 props
 * rcc -> 创建类组件
 *
 */

class App10d01 extends React.Component {
  render() {
    return <div className={'app'}>
      Hello React
      <User name={"孙悟空"} age={18} gender={'男'}></User>
    </div>
  }
}

export default App10d01;
