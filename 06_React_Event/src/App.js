
const App = ()=>{
  const clickHandler = (event)=>{
    event.preventDefault(); // 取消默认行为
    event.stopPropagation(); // 取消事件冒泡行为
    alert('哈哈哈');
  };
  return <div style={{margin: '20px'}}>
    <button onClick={()=>{alert(123);}}>点我一下</button>
    <br/>
    <button onClick={clickHandler}>哈哈</button>
    <br/>
    <a href="https://www.baidu.com" onClick={clickHandler}>我是一个超链接</a>
    <br/>
  </div>;
};
export default App;
