import React from 'react';
import MyDate from "./MyDate/MyDate";
import './LogItem.css';
/*
  rsc 可以快捷的生成模板
*/
const LogItem = (props) => {
  // console.log(props);
  //
  return (
    <div className='item'>
      <MyDate date={props.date}></MyDate>
      {/*日志内容容器*/}
      <div className='content'>
        <h2 className='desc'>{props.desc}</h2>
        <div className="time">{props.time}分钟</div>
      </div>
    </div>
  );
};

export default LogItem;
