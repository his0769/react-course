
import LogItem from "./LogItem/LogItem";
import './Logs.css';

// 可以返回一个数组, {} 会负责展开数组
// 或者使用 `[].map(()=>{})`
const optItem = (arr) => {
  const result = []
  for (const item of arr) {
    result.push(<LogItem key={item.desc} desc={item.desc} time={item.time} date={item.date}></LogItem>)
  }
  return result;
};

/* 日志容器 */
const Logs = () => {
  const logsData = [
    {date: new Date(2022, 1, 20), desc:'学习济阳神功', time: 20},
    {date: new Date(2022, 4, 23), desc:'学习九阴真经', time: 90},
    {date: new Date(2022, 7, 11), desc:'学习蛤蟆功', time: 120},
  ];
  return <div className='logs'>
    {/*
      可以直接在子组件上设置属性
      可以传递函数
    */}
    {/*{logsData.map(item => <LogItem key={item.desc} desc={item.desc} time={item.time} date={item.date}></LogItem>)}*/}
    {/* props和对象属性名一致的话，可以直接使用 ...展开*/}
    {logsData.map(item => <LogItem key={item.desc} {...item}></LogItem>)}
    {/*{optItem(logsData)}*/}
  </div>;
};

export default Logs;
